[width="100%",cols="20%,20%,60%",options="header",]
|===
|Column Name |Values |Description
|*TransactType* |0 |Add, Edit or Delete

|*TransactUserID* |1 |Which user that transact performs

|*TransactDateTime* |2017-06-21 10:21:00 |Last time of Add or Delete to
the table

|*MenuID* |mnuWageTypeBasedTimesheet |To identify the screen data return
to the table

|*PaySheetOTDetailsID* |5198 |Id of the batch (_PrimaryKey_)

|*PaySheetEmpDateID* |7539486 |Primary key of PRL_Payrollsheet02

|*EntityID* |NULL | 

|*OTTypeEntityID* |685 |Entity ID from org_Entitymaster (id of entity
saved in card master)

|*OTInMinutes* |60 |OT given through screen is saved in minutes.
|===

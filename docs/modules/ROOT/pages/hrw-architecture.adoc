= HRW Architecture
:description: This section guides you in setting up your workstation in order to start with development.

HR Works follows a three-tier architecture as follows:

[width="97%",cols="4,17,67"]
|====
|1.
|*Presentation Tier*
|The presentation tier consists of *Windows Presentation Foundation* to develop the UI and *Microsoft .NET Framework* as the software framework.

|2.
|*Application Tier*
|The application tier uses *C# and ASP.NET* and consists of system logic and back-end data processing.

|3.
|*Data Tier*
|The data tier supports both *Oracle* and *Microsoft SQL* as *RDBMS*. It uses *NHibernate* which is an *Object Relational Mapper* (*ORM*).
|====

== System Architecture
[graphviz]
....
digraph foo {
compound=true;
ranksep=1

subgraph cluster_all {
label="Multitier architecture"
Users [shape=tab]

subgraph cluster_presentation {
label="Presentation Layer"
"User Interface" [shape=tab]
"Presentation Logic" [fillcolor=yellow, style="rounded,filled", shape=tab]
        }

Users -> "User Interface" [lhead=cluster_presentation]

subgraph cluster_app {
label="Application Tier"
node[shape=tab]
"Application Logic"
"Framework"
}

"User Interface" -> "Application Logic" [lhead=cluster_app,ltail=cluster_presentation,style=dashed]

subgraph cluster_data {
label="Data Tier"
node[shape=tab]
"Data Storage"
}

"Application Logic" -> "Data Storage" [lhead=cluster_data,ltail=cluster_app,style=dashed]
    }
}
....

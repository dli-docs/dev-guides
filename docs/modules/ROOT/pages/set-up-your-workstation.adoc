= Set up Development Environment
:chapter: 4
:sectnums:
:sectnumoffset: 3
:leveloffset: 1
:toc: macro
:imagesdir: ../../assets/images

This article helps you in setting up your development environment or workstation in order to start working on HR Works, HR Works Plus, and Mobile App development.

{empty} +

toc::[]

= Prerequisites
[width="90%",cols="30,60",options="header"]
|===
|Category
|Description

|*System Requirements*
a|* *Operating System*: Windows, Windows Server

* *Processor*: Core 2 Duo or Athlon X2 at 2.4 GHz or higher

* *Memory*: 4+GB RAM, 2+GB free hard disk space

|*Software Requirements*
a|* .NET Framework
* MS Office (for integration purpose and report generation)

|*Development Tools*
a|* *Subversion (SVN) client*: https://tortoisesvn.net/downloads.html[Tortoise SVN,window="_blank"]
* Subversion integration plug-in for Visual Studio (https://www.visualsvn.com/visualsvn/download/[VisualSVN for Visual Studio,window="_blank"])
* *IDE*: Visual Studio Professional/ Ultimate
* *UI controls, and tools*: DevExpress controls for Visual Studio
* *Database Management System*: Oracle/ MS SQL Server
|===

= Get the source code

The source code of HR Works resides in DLI's remote SVN repository (repo). To get the source files on your PC, you've to checkout the source or in other words create a working copy of the source code from the SVN repository.

NOTE: Before you proceed with the steps below, make sure you've installed the https://tortoisesvn.net/downloads.html[*Tortoise SVN client*,window="_blank"] on your PC.

To create a working copy on your PC using *Tortoise SVN*, follow these steps:

1. Create an empty directory on your PC.
2. Right-click on the directory and then click the **SVN Checkout** option.
+
image::svn/set-up-your-workstation-svn-01.png[]
+
The *Checkout* dialog box appears.

3. In the **URL of repository** field, provide the DLI's SVN repository URL.
+
image::svn/set-up-your-workstation-svn-02.png[]

4. In the *Checkout* dialog box that appears, provide your SVN username and password.
5. Browse and select the repository you want to check out.
6. Wait until all the source files are pulled from the repository to your PC.
7. Once done, click **OK**.

include::setting-up-database.adoc[]

= Configure the IDE

Follow these steps to set up your *Visual Studio IDE*:

1. Install DeveXpress controls
2. Subversion integration plug-in for Visual Studio (https://www.visualsvn.com/visualsvn/download/[VisualSVN for Visual Studio,window="_blank"])

`To be developed`

= Develop/ Collaborate with other developers

[NOTE]
====
Before you proceed with the steps below, make sure you've done the following.

* Created a working copy of the source files.
+
Refer to the <<get-the-source-code, Get the source code>> section for more details.
+
* Installed *Visual Studio*.

* Installed *DevExpress UI Controls*.

* Added the https://www.visualsvn.com/visualsvn/download/[*VisualSVN plug-in*,window="_blank"] to *Visual Studio*.
+
The VisualSVN plug-in integration makes it easy for you to manage versions of the code in the subversion repo within your Visual Studio project environment.
+
* Configured the HR Works database.
+
Refer to the <<configure-hr-works-database-connection, Configure HR Works database connection>> section for more details.
====

To get started with HRW development and to collaborate with developers, follow the steps below.

. Open the solution file (SLN).
+
The solution file resides in the working copy directory, which you have checked out from the SVN repo.
+
. If you already have configured the database, skip this step.
+
Open the *HRWorks.Win.UI.exe.config* file, provide the database connection string values, and then save it.
. Now that you have set up the project files, make required changes to the source files, DB, etc and save your changes.
. The next step is to commit your changes to the remote repository using the VisualSVN plug-in.
+
Refer to the following topics for more details on using the VisualSVN plugin:
+
** https://www.visualsvn.com/visualsvn/getting-started/#commit-solution-to-subversion[Commit Solution to Subversion | Getting Started | VisualSVN for Visual Studio (opens in a new tab),window="_blank"]
** https://www.visualsvn.com/visualsvn/getting-started/#basic-work-cycle[Basic Work Cycle | Getting Started | VisualSVN for Visual Studio (opens in a new tab),window="_blank"]

= Getting updates


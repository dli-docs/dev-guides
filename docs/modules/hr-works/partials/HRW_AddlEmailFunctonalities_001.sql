IF OBJECT_ID('HRW_AddlEmailFunctonalities_001') IS NOT NULL
	DROP PROCEDURE HRW_AddlEmailFunctonalities_001
GO
CREATE PROCEDURE HRW_AddlEmailFunctonalities_001
(
	@Data XML = ''
)
AS
DECLARE @XML AS XML, 
		@hDoc AS INT, 
		@SQL NVARCHAR (MAX),
		@SlNo INT = 1,
		@MaxNo INT 
DECLARE @Requests TABLE
(
	Request00_ID BIGINT,
	CompanyCode VARCHAR(10),
	EmployeeID BIGINT,
	RequestTypeID BIGINT,
	DelayReq INT
)
DECLARE @HRW_EmailTypes TABLE 
(
	ID INT IDENTITY,
	RequestType XML,
	EmailTypeID BIGINT,
	RequestTypeID BIGINT NULL
)
INSERT INTO @HRW_EmailTypes
(
	RequestType, EmailTypeID
)
SELECT
	RequestTypeID, EmailTypeID
FROM HRW_EmailTypes

SELECT @MaxNo = @@ROWCOUNT
WHILE @SlNo <= @MaxNo
BEGIN 
	SELECT 
		@XML = RequestType
	FROM @HRW_EmailTypes
	WHERE ID = @SlNo
	EXEC sp_xml_preparedocument @hDoc OUTPUT, @XML

	UPDATE @HRW_EmailTypes
	SET RequestTypeID = (
							SELECT 
								ID
							FROM OPENXML(@hDoc, 'RequestTypes/RequestType')
							WITH 
							(
								ID BIGINT
							)
						)
	WHERE ID = @SlNo
	EXEC sp_xml_removedocument @hDoc

	SET @SlNo = @SlNo + 1
END

INSERT INTO @Requests
SELECT
	R0.Request00_ID, HE.CompanyCode,R0.EmployeeID, RequestTypeID, 
	DATEDIFF(DD,NULLIF(R0.SubmittedDateTime,SCD.ApprovalDateTime),GETDATE()) AS DelayReq
FROM HSS_Request00 R0
		JOIN
	HRW_Employee HE ON
		HE.EmployeeID = R0.EmployeeID
		OUTER APPLY
	(
		SELECT
			TOP 1 ROW_NUMBER()OVER( ORDER BY ApprovalDateTime DESC) AS SlNo, ApprovalDateTime, ApprovalStatus
		FROM HSS_Request01_AWF R1W
		WHERE R0.Request00_ID = R1W.Request00_ID
			AND R0.RequestStatus = 'S'
	) SCD
WHERE RequestStatus IN ('S','N')	

UPDATE R0
	SET R0.RequestStatus = 'C'
FROM HSS_Request00 R0
		JOIN
	@Requests R ON
		R0.Request00_ID = R.Request00_ID
WHERE R.DelayReq >= 3

INSERT INTO HRW_AlertSchedule
(
	TransactType, TransactUserID, CompanyCode, TransactDateTime, MenuID, ScheduledTime, Status, EmailTypeID, Request00_ID
)
SELECT
	0, 0, CompanyCode, GETDATE(), 'mnuEmailattendanceservice', CONVERT( VARCHAR(5), GETDATE(),108), 'N', HET.EmailTypeID, RS.Request00_ID
FROM @Requests RS
		LEFT JOIN
	@HRW_EmailTypes HET ON
		HET.RequestTypeID = RS.RequestTypeID
WHERE DelayReq > 0



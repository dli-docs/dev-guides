[cols="17,83"]
.Configuring rules for Attendance Exception email alerts
|===
|*Settings/ Field name*|*Description*

|*Email Sending Frequency*
a|To specify the frequency at which HR Works should send the alert, select any of the following options as required:

.. Daily
.. Weekly
.. Monthly

*Notification Day*:

If you select Weekly or Monthly, then the *Notification Day* field appears where you must specify the days on which you want HR Works to send the alert.

[cols="1,2"]
!===
! IF ! Then

! *Weekly* is selected
! The *Notification Day* dropdown field shows the week days from which you can select the day/s on which the alert needs to be sent

! *Monthly* is selected
! The *Notification Day* dropdown field shows the days in a month from which you can select the dates on which the alert needs to be sent.
!===

|*Email Sending Time*
|Specify the time at which you want HR Works to send the alerts. You can enter any number of time blocks here.

|*Email Template*
|Select the template you want to use for the alert you're creating.

|*Email Subject*
|Enter the text that you want to show in the subject line of the email alert.

|*Email Sending Period*
a|Select any of the following options.

* If you select *Current Day*, then the alerts are sent with the time attendance exception data of the current day. It contains only the _Late In_ or _Absent_ events from time booking records of the employee.
* If you select *Previous Day*, then the alerts are sent with the attendance exception data of the previous day. Relevant exception details of the employee are retrieved from the calculated attendance.

|*Recipient*
a|Select any of the following options:

* If you want HR Works to send alerts to the respective employee, then select *Employee*.
+
If you want to exclude any employees from receiving the alert, then select the *Exclusion Employees* tab on the bottom pane of the *Email Settings* screen, and then select the employees you want to exclude.
+
* If you want HR Works to send alerts to any organization roles, then select *Organizational Role*, and then do the following.
+
. From the *Organization Role* dropdown field that appears, select the organization role to which you want to send the alert.
+

If any organization role as defined in the 5th point of the *Specify Organization roles* table is selected, from the *Recipient Email Entity* field, you can select the email field corresponding to that organization role.
+
. From the *Email Sending Selection* dropdown field, select any of the positional entity types and then from the next dropdown field select the positional entity.
+

Alerts are sent to the organization role corresponding to the positional entity/ entities that you select here.
+
. If you want HR Works to send copy of the alerts as CC, then do any of the following as required.
** To send alerts tp the positional entity/ entities, then from the *CC Email Sending Selection* field, select the respective positional entity.
+
*OR*
+
** To specify the email address, select _Common_ and then enter the email addresses to which you want the alerts to be sent.
+
If you want to add more than one email address, use a comma to separate them.
+

[NOTE]
====
* If you've selected an entity-level parameter from the *Organizational Role* dropdown, then the corresponding entity type must be selected from the *Email Sending Selection* dropdown.
+
*Example*:
+
** Your _Department_ entity has _Department Head_ parameter with its datatype set as _Employee (This Company)_ or _Employee (All Company)_.

** And the _Department Head_ parameter is the Organization Role you've selected as the recipient for the alert.
+
Then you must select _Department_ as your *Email Sending Selection* value.
+
You can then filter the required entities - the Card Master values of the _Department_ entity, from selection box that appears below the *Email Sending Selection* field.
+
* If you've selected an employee-level parameter from the *Organizational Role* dropdown, then employees are filtered based on the entity types, which you select from the *Email Sending Selection*'s *Entity Type* selection box.
+
For example, if your _Employee Fields_ has a parameter _Line Manager_ with _Employee (This Company)_ or _Employee (All Company)_ as its datatype, and if that you've set _Line Manager_ parameter as the recipient Organization Role in the *Email Settings*, then you can select any entity types you want as the Email Sending Selection.
====

|*Enable Mailing*
|To activate the alert, you must select this option.

|*Testing SMTP Connection*
a|If you want to test if the SMTP and other email configurations are working properly, then do the following:

* In the *Email Receiver* field, enter an email address to which you want to send the test mail, and then click *Test*.
+
If your SMTP, email, and alert settings and configurations are correct, the system will send a test mail to the email address you provided. If this does’t work, then check the email address you provided or review your *Outgoing Email Server* configuration.
+
For more details on setting up outgoing mail server in HR Works, see to *Configure Outgoing Email Server* section in this document.
|===